//
//  ListPlayingSong.h
//  Music App
//
//  Created by Vuong Hoang Son on 5/24/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface ListPlayingSong : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *contentTable;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundSong;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSong;
@property (weak, nonatomic) IBOutlet UILabel *lblArtist;
@property (weak, nonatomic) IBOutlet UILabel *lblAlbum;
@property (weak, nonatomic) IBOutlet UILabel *lblShuffle;
@property (weak, nonatomic) IBOutlet UIImageView *iconShuffle;

- (IBAction)backToHomeView:(id)sender;
@end
