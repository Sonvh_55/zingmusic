//
//  PostsViewController.h
//  SoundFree
//
//  Created by Rajkumar Sharma on 08/11/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface PostsViewController : UIViewController <UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *contentTable;
@end
