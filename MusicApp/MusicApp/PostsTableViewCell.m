//
//  PostsTableViewCell.m
//  SoundFree
//
//  Created by Rajkumar Sharma on 08/11/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "PostsTableViewCell.h"

@implementation PostsTableViewCell

- (void)awakeFromNib {
    
    self.img1.layer.borderColor = [UIColor whiteColor].CGColor;
    self.img1.layer.borderWidth = 0.5;
    self.img1.layer.cornerRadius = 10;
    
    self.img2.layer.borderColor = [UIColor whiteColor].CGColor;
    self.img2.layer.borderWidth = 0.5;
    self.img2.layer.cornerRadius = 10;
    
    self.img3.layer.borderColor = [UIColor whiteColor].CGColor;
    self.img3.layer.borderWidth = 0.5;
    self.img3.layer.cornerRadius = 10;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
