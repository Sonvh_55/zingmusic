#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"
@interface HomeViewController : UIViewController <AVAudioPlayerDelegate>{
    IBOutlet UIImageView *imgSong;
    IBOutlet UIImageView *imgSong1;
    IBOutlet UIImageView *imgSong2;
    IBOutlet UIButton *btnPlay;
    NSString *imgName;
    int currentIndex;
    AppDelegate *appDelegate;
    IBOutlet UILabel *lblTrack;
    IBOutlet UILabel *lblArtist;
    IBOutlet UILabel *lblAlbum;
    __weak IBOutlet UIButton *btSuffle;
    __weak IBOutlet UIButton *btRepeat;
    __weak IBOutlet UIButton *btShare;
    __weak IBOutlet UIButton *btDownload;
    __weak IBOutlet UILabel *lbTotalTime;
    __weak IBOutlet UILabel *playedTime;
    __weak IBOutlet UIImageView *slideTimeOver;
    __weak IBOutlet UIImageView *timeSeeder;
    __weak IBOutlet UIImageView *timeTotalBar;
    __weak IBOutlet UISlider *volumneSlider;
    
}

@property (nonatomic, weak) IBOutlet UIScrollView *contentScroll;
@property (weak, nonatomic) IBOutlet UISlider *playingProgress;
@property (nonatomic, strong) AVPlayer *avPlayer;
@property (nonatomic,strong) AVAsset * currentAsset;
@property (nonatomic,strong) AVPlayerItem * streamingItem;



- (IBAction)changeVolume:(id)sender;
- (void)playSongAtIndex:(int)index;


@end
