//
//  PostsViewController.m
//  SoundFree
//
//  Created by Rajkumar Sharma on 08/11/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "PostsViewController.h"
#import "PostsTableViewCell.h"

@interface PostsViewController (){
    NSArray *arrOfTitles;
    NSInteger arrNumberOfSong[4];
}
@end

@implementation PostsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    arrOfTitles = [NSArray a]
    // Do any additional setup after loading the view.
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    arrOfTitles = @[@{@"title":@"Nhạc cá nhân"}];
    
    arrNumberOfSong[0]= (UIAppDelegate.localSongArray)?[UIAppDelegate.localSongArray count]: 0;
    [self.contentTable reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PostsTableViewCell *cell = (PostsTableViewCell *)[self.contentTable dequeueReusableCellWithIdentifier:@"PostCell"];
    cell.lblTitle.text = [[arrOfTitles objectAtIndex:indexPath.row] objectForKey:@"title"];
    cell.lblSubTitle.text = [NSString stringWithFormat:@"%li", (long)arrNumberOfSong[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:@"PushToTrending" sender:nil];
   
}


@end
