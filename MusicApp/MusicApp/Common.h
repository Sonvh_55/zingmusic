//
//  Common.h
//  RemoteInventory
//
//  Created by Hien on 7/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIAlertView+Blocks.h"

@interface Common : NSObject

+ (void)showWarningWithMessage:(NSString*)message;

+ (void)showErrorWithMessage:(NSString*)message;

+ (void)showNetworkErrorMessage;
+ (void)showRequestErrorMessage ;

+ (BOOL)isHasInternetConnection;
+ (BOOL)checkInternetConnection;
+ (BOOL) isCurrentSongDowloaded:(NSString *) songId;

@end