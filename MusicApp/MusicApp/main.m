//
//  main.m
//  MusicApp
//
//  Created by Huy Hoang on 5/11/15.
//  Copyright (c) 2015 SonVuongHoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
