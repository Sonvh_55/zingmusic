//
//  ArtistViewCollectionCell.h
//  Music App
//
//  Created by Vuong Hoang Son on 5/15/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistViewCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *artistImg;
@property (weak, nonatomic) IBOutlet UILabel *artistName;

@end
