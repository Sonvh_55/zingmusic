//
//  ArtistInfoViewController.h
//  Music App
//
//  Created by Vuong Hoang Son on 5/16/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "SCUI.h"
#import "SCAPI.h"
#import "AppDelegate.h"

@interface ArtistInfoViewController : UIViewController

@property (strong, nonatomic) NSString *artistId;

@property (weak, nonatomic) IBOutlet UIImageView *artisAvatar;
@property (weak, nonatomic) IBOutlet UILabel *artistName;
@property (weak, nonatomic) IBOutlet UILabel *artistBirthday;
@property (weak, nonatomic) IBOutlet UILabel *artistLocation;
@property (weak, nonatomic) IBOutlet UITextView *artistInfo;
- (IBAction)backToArtistProfileView:(id)sender;
@end
