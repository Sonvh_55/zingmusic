//
//  ArtistListViewController.h
//  Music App
//
//  Created by Vuong Hoang Son on 5/15/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ArtistListViewController : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource >

@property (nonatomic, weak) IBOutlet UITableView *contentTable;
@property (weak, nonatomic) IBOutlet UICollectionView *contentCollection;
- (IBAction)reloadNetworkButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *networkErrorView;
@end
