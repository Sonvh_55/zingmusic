
#import <UIKit/UIKit.h>

#define THEMECOLOR [UIColor colorWithRed:255.0/255.0 green:131.0/255.0 blue:44.0/255.0 alpha:1]
#define ListPlayistOfflineFile @"ListPlayistOfflineFile.data"

/*-----------ZING_API------------*/
#define Z_DOMAIN_API @"http://api.mp3.zing.vn"
#define Z_DOMAIN_IMAGE @"http://image.mp3.zdn.vn"
#define Z_DOMAIN_MP3 @"http://mp3.zing.vn"


#define Z_BXH_VIETNAM @"http://api.mp3.zing.vn/api/mobile/charts/getchartsinfo?requestdata={\"length\":40,\"id\":1,\"start\":0}"
#define Z_BXH_AUMY @"http://api.mp3.zing.vn/api/mobile/charts/getchartsinfo?requestdata={\"length\":40,\"id\":50,\"start\":0}"
#define Z_BXH_HANQUOC @"http://api.mp3.zing.vn/api/mobile/charts/getchartsinfo?requestdata={\"length\":40,\"id\":51,\"start\":0}"

#define Z_SEARCH_SONG_API @"http://api.mp3.zing.vn/api/mobile/search/song?requestdata={\"length\":20,\"start\":1,\"q\":\"%@\",\"sort\":\"hot\"}"

#define Z_LIST_ARTIST @"http://api.mp3.zing.vn/api/mobile/artist/getartistbygenre?requestdata={\"length\":60,\"id\":0,\"start\":0}"
#define Z_SONG_BY_ARTIST @"http://api.mp3.zing.vn/api/mobile/artist/getsongofartist?requestdata={\"length\":20,\"id\":\"%@\",\"start\":0}"


//#define Z_SEARCH_SONG_API


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSArray *soundArray;
@property (nonatomic, strong) NSMutableArray *localSongArray;
@property (nonatomic, strong) NSMutableArray *listFavourites;


@property (nonatomic, assign) NSInteger currentPlayingIndex;
@property (nonatomic, strong) NSString *currentTitleSong;
@property (nonatomic, strong) NSString *currentSongArtist;
@property (nonatomic, strong) NSString *currentSongAlbumName;
@property (nonatomic, strong) UIImage *currentBackgroundSong;

// API Link
@property (nonatomic, assign) BOOL isUsingZingAPI;
@property (strong, nonatomic) NSString *DIRECTORYPATH;
@property (strong, nonatomic) NSString *LISTOFFLINEPATH;

@end

