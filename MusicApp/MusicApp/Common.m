//
//  Common.m
//  RemoteInventory
//
//  Created by Hien on 7/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Common.h"
#import  "AppDelegate.h"
#import "Reachability.h"
#import "LocalSong.h"

@implementation Common

+ (void)showWarningWithMessage:(NSString*)message {
    [UIAlertView showWarningWithMessage:message handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
    }];
    
}

+ (void)showErrorWithMessage:(NSString*)message {
    [UIAlertView showErrorWithMessage:message handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
    }];
}

+ (void)showRequestErrorMessage {
    [Common showErrorWithMessage:@"Error Connecting To Webservice"];
}

+ (void)showNetworkErrorMessage {
    [Common showErrorWithMessage:@"The Internet connection appears to be offline. Please try again."];
}

+ (BOOL)checkInternetConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        [Common showErrorWithMessage:@"The Internet connection appears to be offline. Please try again."];
        
        return !(networkStatus == NotReachable);
    }
    return YES;
}
+ (BOOL)isHasInternetConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
             return !(networkStatus == NotReachable);
    }
    return YES;
}
+ (BOOL) isCurrentSongDowloaded:(NSString *) songId {
    NSInteger total = [UIAppDelegate.localSongArray count];
    if (total == 0) {
        return NO;
    }
    for (NSInteger i =0; i< total; i++) {
        LocalSong *song = [UIAppDelegate.localSongArray objectAtIndex:i];
        if ([songId isEqualToString: song.songId]) {
            return YES;
        }
    }
    return NO;
}



@end
