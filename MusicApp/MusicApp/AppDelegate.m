
#import "AppDelegate.h"
#import "LocalSong.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize  DIRECTORYPATH, LISTOFFLINEPATH;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.localSongArray =[[NSMutableArray alloc] init];
    self.isUsingZingAPI = YES;
    [self updateDirectoryPath];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [[UITabBar appearance] setTintColor:THEMECOLOR];
    [[UITabBar appearance] setShadowImage:[UIImage imageNamed:@"transparent.png"]];
    
    UITabBarController *tabBar = (UITabBarController *)self.window.rootViewController;
    tabBar.selectedIndex = 0;
    return YES;
}

- (void)updateDirectoryPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    DIRECTORYPATH = [documentsDirectory stringByAppendingPathComponent:@"MusicFolderStorage"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:DIRECTORYPATH])
        [[NSFileManager defaultManager] createDirectoryAtPath:DIRECTORYPATH withIntermediateDirectories:NO attributes:nil error:nil];
    LISTOFFLINEPATH = [documentsDirectory stringByAppendingPathComponent: ListPlayistOfflineFile];
    if ([[NSFileManager defaultManager] fileExistsAtPath:LISTOFFLINEPATH]) {
        self.localSongArray = (NSMutableArray*) [NSKeyedUnarchiver unarchiveObjectWithFile:LISTOFFLINEPATH];
        if (!self.localSongArray) {
            self.localSongArray = [NSMutableArray array];
        }
    }
    else {
        [[NSFileManager defaultManager] createFileAtPath:LISTOFFLINEPATH contents:nil attributes:nil];
    }
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    //if it is a remote control event handle it correctly
    if (event.type == UIEventTypeRemoteControl) {
        NSDictionary *playingChanged;
        if (event.subtype == UIEventSubtypeRemoteControlPlay) {
            playingChanged = [NSDictionary dictionaryWithObject:@"play"
                                                         forKey:@"playingChanged"];
        } else if (event.subtype == UIEventSubtypeRemoteControlPause) {
            playingChanged = [NSDictionary dictionaryWithObject:@"pause"
                                                         forKey:@"playingChanged"];
        } else if (event.subtype == UIEventSubtypeRemoteControlTogglePlayPause) {
            playingChanged = [NSDictionary dictionaryWithObject:@"toggle"
                                                         forKey:@"playingChanged"];
        } else if (event.subtype == UIEventSubtypeRemoteControlNextTrack) {
            playingChanged = [NSDictionary dictionaryWithObject:@"next"
                                                         forKey:@"playingChanged"];
        } else if (event.subtype == UIEventSubtypeRemoteControlPreviousTrack) {
            playingChanged = [NSDictionary dictionaryWithObject:@"prev"
                                                         forKey:@"playingChanged"];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MusicPlayingChanged" object:self userInfo:playingChanged];
    }
}

@end
