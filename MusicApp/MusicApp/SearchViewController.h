//
//  SearchViewController.h
//  Music App
//
//  Created by Vuong Hoang Son on 5/24/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SCUI.h"
#import "SCAPI.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "SWTableViewCell.h"

@interface SearchViewController : UIViewController <UITextFieldDelegate > {
    HomeViewController *homeViewController;
}

@property (nonatomic, weak) IBOutlet UITableView *contentTable;


@end
