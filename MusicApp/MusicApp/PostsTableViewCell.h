//
//  PostsTableViewCell.h
//  SoundFree
//
//  Created by Rajkumar Sharma on 08/11/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *img1;
@property (nonatomic, weak) IBOutlet UIImageView *img2;
@property (nonatomic, weak) IBOutlet UIImageView *img3;

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblSubTitle;

@end
