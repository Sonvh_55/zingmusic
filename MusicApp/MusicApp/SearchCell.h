//
//  SearchCell.h
//  Music App
//
//  Created by Vuong Hoang Son on 5/24/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "SWTableViewCell.h"

@interface SearchCell : SWTableViewCell <SWTableViewCellDelegate>

@property (strong, nonatomic) NSString *linkTrack;
@property (strong, nonatomic) NSString *idTrack;
@property (weak, nonatomic) IBOutlet UILabel *lbLike;
@property (weak, nonatomic) IBOutlet UILabel *lbListen;
@property (weak, nonatomic) IBOutlet UILabel *lbDownload;
@property (weak, nonatomic) IBOutlet UILabel *lbAlbum;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *icFavorite;
@property (weak, nonatomic) IBOutlet UIImageView *icDownloaded;

@end
