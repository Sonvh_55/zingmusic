//
//  ArtistListViewController.m
//  Music App
//
//  Created by Vuong Hoang Son on 5/15/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "ArtistListViewController.h"
#import "MBProgressHUD/MBProgressHUD.h"
#import "Common.h"
#import "SearchCell.h"
#import "ArtistViewCollectionCell.h"
#import "ArtistProfileViewController.h"

static NSString *artistProfileIndentifier = @"artistProfile";

@interface ArtistListViewController ()

@property(nonatomic, strong) NSMutableArray *listArtistArray;
@property(nonatomic, assign) NSInteger currentIndex;

@end

@implementation ArtistListViewController

#pragma mark -VIEW LIFE CYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _listArtistArray = [NSMutableArray array];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self getList:@"http://api.mp3.zing.vn/api/mobile/artist/getartistbygenre?requestdata={\"length\":30,\"id\":0,\"start\":0}"];
}
#pragma mark - API GET LIST ARTIST
- (void) getList: (NSString*) url{
    if (![Common isHasInternetConnection]) {
        _networkErrorView.hidden = NO;
        return;
    }
    [SCRequest performMethod:SCRequestMethodGET
                  onResource:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
             usingParameters:nil
                 withAccount:[SCSoundCloud account]
      sendingProgressHandler:nil
             responseHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 
                 NSError *jsonError;
                 NSDictionary *responseResult;
                 if (data) responseResult = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                 else {
                     return;
                 }
                 if (!jsonError) {
                     _listArtistArray = [NSMutableArray arrayWithArray:(NSArray *)[responseResult objectForKey:@"docs"]] ;
                     [self.contentCollection reloadData];
                     [self.contentTable reloadData];
                 }
                 else {
                     NSLog(@"%@", error.localizedDescription);
                 }
             }];
}

#pragma mark - API SEARCH HANDLE

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length == 0) {
        return YES;
    }
    NSString *searchBarText = [self normalVi:textField.text];
    NSLog(@"Text Normaled: %@", searchBarText);
    [self searchSongWithText:searchBarText];
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *searchBarText = [self normalVi:searchBar.text];
    NSLog(@"Text Normaled: %@", searchBarText);
    [self searchSongWithText:searchBarText];
}
- (void) searchSongWithText: (NSString *) text {

    if(UIAppDelegate.isUsingZingAPI) {
        [self getList: [NSString stringWithFormat:@"http://api.mp3.zing.vn/api/mobile/search/song?requestdata={\"length\":20,\"start\":1,\"q\":\"%@\",\"sort\":\"hot\"}",text]];
    }
    else {
        [self getList:[NSString stringWithFormat:@"https://api.soundcloud.com/tracks.json?q=%@&format=json&client_id=05f12042d51e307cc1488b504fdc0646", text]];
    }
}
- (NSString*) normalVi: (NSString*) string{
    if ([string isEqual:[NSNull null]]){
        return @"";
    }
    NSString *special = @"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰự";
    
    NSString *replace = @"AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUu";
    
    NSString *result = [[NSString alloc] initWithFormat:@"%@", string];
    
    for (int i = 0; i < special.length; i++){
        NSString* s1 = [special substringWithRange:NSMakeRange(i, 1)];
        NSString *s2 = [replace substringWithRange:NSMakeRange(i, 1)];
        
        result = [result stringByReplacingOccurrencesOfString: s1
                                                   withString: s2];
    }
    return result;
}

#pragma mark - TABLE INTERFACE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ([_listArtistArray count] - 8);
  
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ([_listArtistArray count] == 0)?0:1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _currentIndex = indexPath.row;
    [self performSegueWithIdentifier:artistProfileIndentifier sender:self];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchCell *cell = (SearchCell *) [self.contentTable dequeueReusableCellWithIdentifier:@"ArtistCell"];
    
    NSDictionary *item = [_listArtistArray objectAtIndex: (indexPath.row + 8) ];
    NSString *avatar = item [@"avatar"];
    NSString *avatarLink = [NSString stringWithFormat:@"%@/%@", Z_DOMAIN_IMAGE, avatar] ;
    if (avatarLink != nil
        && ![avatarLink isEqual: [NSNull null]]
        ){
        NSURL *imageURL = [NSURL URLWithString:avatarLink];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                UIImage *background = [UIImage imageWithData:imageData];
                cell.ivAvatar.image = background;
                cell.ivAvatar.contentMode = UIViewContentModeScaleAspectFit;
            });
        });
    }
    cell.lbTitle.text = item[@"name"];
    return cell;
    
}

#pragma mark -  COLLECTION VIEW HANDLE

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return ([_listArtistArray count] == 0)?0:1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 8;
}
-(void)collectionView:(UICollectionView*)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    _currentIndex = indexPath.row;
    [self performSegueWithIdentifier:artistProfileIndentifier sender:self];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CellCollection";
    ArtistViewCollectionCell *cell = (ArtistViewCollectionCell *) [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *item = [_listArtistArray objectAtIndex: indexPath.row];
    NSString *avatar = item [@"avatar"];
    NSString *avatarLink = [NSString stringWithFormat:@"%@/%@", Z_DOMAIN_IMAGE, avatar] ;
    if (avatarLink != nil
        && ![avatarLink isEqual: [NSNull null]]
        ){
        NSURL *imageURL = [NSURL URLWithString:avatarLink];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                UIImage *background = [UIImage imageWithData:imageData];
                cell.artistImg.image = background;
                cell.artistImg.contentMode = UIViewContentModeScaleAspectFit;
            });
        });
    }
    cell.artistName.text = item[@"name"];
    return cell;
}

#pragma mark - PREPARE FOR SEGUE

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:artistProfileIndentifier])
    {
        NSString *artistId  = [_listArtistArray objectAtIndex:_currentIndex][@"artist_id"];
        NSString *cover = [_listArtistArray objectAtIndex:_currentIndex][@"cover"];
        NSString *coverLink = [NSString stringWithFormat:@"%@/%@", Z_DOMAIN_IMAGE, cover];
        ArtistProfileViewController *vc = [segue destinationViewController];
        // Pass any objects to the view controller here, like...
        vc.artistId = artistId;
        vc.artistAvaLink = coverLink;
        
    }
    
}


- (IBAction)reloadNetworkButtonTapped:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if ([Common isHasInternetConnection]) {
        _networkErrorView.hidden = YES;
        [self getList:@"http://api.mp3.zing.vn/api/mobile/artist/getartistbygenre?requestdata={\"length\":30,\"id\":0,\"start\":0}"];
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];

}
@end
