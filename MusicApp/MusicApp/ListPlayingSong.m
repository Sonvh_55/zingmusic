//
//  ListPlayingSong.m
//  Music App
//
//  Created by Vuong Hoang Son on 5/24/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "ListPlayingSong.h"
#import "TrendingTableViewCell.h"
#import "LocalSong.h"
#import "OnlineSong.h"


@interface ListPlayingSong  ()

@end


@implementation ListPlayingSong

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePlayList)
                                                 name:@"IndexChanged"
                                               object:nil];
    [self updateUIForCurrentSong];
}
- (void) viewWillAppear:(BOOL)animated {
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [UIAppDelegate.soundArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TrendingTableViewCell *cell = (TrendingTableViewCell *)[self.contentTable dequeueReusableCellWithIdentifier:@"TrendingCell"];
    if (indexPath.row == UIAppDelegate.currentPlayingIndex) {
        cell.lblSerial.hidden = YES;
        cell.imgPlay.hidden = NO;
    } else {
        cell.lblSerial.hidden = NO;
        cell.imgPlay.hidden = YES;
        cell.lblSerial.text = [NSString stringWithFormat:@"%ld",(long)(indexPath.row + 1)];
    }
    if ([[UIAppDelegate.soundArray objectAtIndex:indexPath.row] isKindOfClass:[LocalSong class]]) {
        LocalSong *song = [UIAppDelegate.soundArray objectAtIndex:indexPath.row];
        cell.lblTime.text = [self formatTime: song.duration];
        cell.lblTrack.text = song.title;
    }
    else if ([[UIAppDelegate.soundArray objectAtIndex:indexPath.row] isKindOfClass:[OnlineSong class]]) {
        NSLog(@"%@", [UIAppDelegate.soundArray objectAtIndex:indexPath.row] );
        OnlineSong *song = [UIAppDelegate.soundArray objectAtIndex:indexPath.row];
        cell.lblTime.text = [self formatTime:song.duration];
        cell.lblTrack.text = song.title;
    }
    return cell;
}
- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIAppDelegate.currentPlayingIndex = indexPath.row;
    NSDictionary *playingSongAtIndex = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%li", (long)UIAppDelegate.currentPlayingIndex] forKey:@"PlayAtIndex"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MusicPlayingChanged" object:self userInfo:playingSongAtIndex];
    [self updateUIForCurrentSong];
    [_contentTable reloadData];
}

- (NSString*) formatTime: (NSString *) duaration{

    int elapsedMinisecond = [duaration intValue];
    NSUInteger h = elapsedMinisecond / 3600;
    NSUInteger m = (elapsedMinisecond / 60) % 60;
    NSUInteger s = elapsedMinisecond % 60;
    if (h > 0){
        return [NSString stringWithFormat:@"%lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
    } else {
        return [NSString stringWithFormat:@"%02lu:%02lu", (unsigned long)m, (unsigned long)s];
    }
}
- (void)updateUIForCurrentSong {
    [_lblTitleSong setText:UIAppDelegate.currentTitleSong];
    [_lblArtist setText:UIAppDelegate.currentSongArtist];
    [_lblAlbum setText:UIAppDelegate.currentSongAlbumName];
    [_backgroundSong setImage:UIAppDelegate.currentBackgroundSong];
    _backgroundSong.contentMode = UIViewContentModeScaleAspectFit;
}
- (IBAction)backToHomeView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) updatePlayList {
    [_contentTable reloadData];
    [self updateUIForCurrentSong];
}

@end
