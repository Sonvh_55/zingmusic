//
//  SearchViewController.m
//  SoundFree
//
//  Created by Rajkumar Sharma on 08/11/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Common.h"
#import "LocalSong.h"
#import "OnlineSong.h"



@interface SearchViewController () <SWTableViewCellDelegate> {
    NSArray *arrButtons;
}

@property (nonatomic, assign) BOOL isSearchSong;
@property (nonatomic, assign) NSIndexPath *currentIndexPath;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) NSMutableData *loadingSongData;
@property (nonatomic, strong) NSMutableArray  *listOnlineSongArray;

@end

@implementation SearchViewController
@synthesize isSearchSong;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _listOnlineSongArray = [[NSMutableArray alloc] init];
    SWTableViewCell *swTableView = [SWTableViewCell new];
    swTableView.delegate = self;
      [self trackList: Z_BXH_VIETNAM];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
/*-------------------------------------*/
#pragma mark - HANDLE SEARCH DELEGATE
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.text.length == 0) {
        return YES;
    }
    NSString *searchBarText = [self normalVi:textField.text];
    NSLog(@"Text Normaled: %@", searchBarText);
    [self searchSongWithText:searchBarText];
    //    searchBarText =  [searchBarText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *searchBarText = [self normalVi:searchBar.text];
    NSLog(@"Text Normaled: %@", searchBarText);
    [self searchSongWithText:searchBarText];
}
- (void) searchSongWithText: (NSString *) text {

    isSearchSong = YES;
    [self trackList: [Z_SEARCH_SONG_API stringByReplacingOccurrencesOfString:@"%@" withString:text]];
}
- (NSString*) normalVi: (NSString*) string{
    if ([string isEqual:[NSNull null]]){
        return @"";
    }
    NSString *special = @"ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰự";
    
    NSString *replace = @"AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUu";
    
    NSString *result = [[NSString alloc] initWithFormat:@"%@", string];
    
    for (int i = 0; i < special.length; i++){
        NSString* s1 = [special substringWithRange:NSMakeRange(i, 1)];
        NSString *s2 = [replace substringWithRange:NSMakeRange(i, 1)];
        
        result = [result stringByReplacingOccurrencesOfString: s1
                                                   withString: s2];
    }
    return result;
}
- (void) trackList: (NSString*) url{
    if (![Common checkInternetConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [SCRequest performMethod:SCRequestMethodGET
                  onResource:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
             usingParameters:nil
                 withAccount:[SCSoundCloud account]
      sendingProgressHandler:nil
             responseHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 
                 NSError *jsonError;
                 NSDictionary *responseResult;
                 if (data)  responseResult = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                 else {
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     return;
                     
                 }
                 if (!jsonError && responseResult) {
                     NSArray *list;
                     [_listOnlineSongArray removeAllObjects];
                     _listOnlineSongArray = [NSMutableArray array];
                 
                         if (isSearchSong) {
                             isSearchSong = NO;
                             list = (NSArray *) [responseResult objectForKey:@"docs"];
                         }
                         else
                             list = (NSArray *) [responseResult objectForKey:@"item"];
                     
 
                     for (NSDictionary *zingItem in list ){
                         OnlineSong *song =  [[OnlineSong alloc] initSongWithZingListDictionary:zingItem];
                         [_listOnlineSongArray addObject:song];
                     }
                     list = nil;
                     [self.contentTable reloadData];
                 }
                 else {
                     [Common showErrorWithMessage:error.localizedDescription];
                 }
             }];
}

/*---------------------------------------*/
#pragma mark - TABLEVIEW

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listOnlineSongArray count];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ([_listOnlineSongArray count] == 0)?0:1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIAppDelegate.soundArray  =  [NSArray arrayWithArray:_listOnlineSongArray];
    HomeViewController *home = [self.tabBarController.viewControllers objectAtIndex:0];
    [self.tabBarController setSelectedViewController:home];
    [home playSongAtIndex: (int)_currentIndex];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SearchCell *cell = (SearchCell *) [self.contentTable dequeueReusableCellWithIdentifier:@"SearchCell"];
    cell.rightUtilityButtons = [self rightUtilityButtons];
    cell.delegate = self;
    if (_listOnlineSongArray != nil
        && _listOnlineSongArray.count > indexPath.row ){
        
        if ([[_listOnlineSongArray objectAtIndex:indexPath.row] isKindOfClass: [OnlineSong class] ])
        {
            OnlineSong *song = [_listOnlineSongArray objectAtIndex:indexPath.row];
            cell.lbAlbum.text = song.artist;
            cell.lbTitle.text = song.title;
            cell.linkTrack = song.link_download;
            cell.lbListen.text = song.total_play;
            cell.icFavorite.hidden = YES;
            if (!song.background) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    NSURL *imageURL = [NSURL URLWithString:song.link_thumbnail];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        song.background = [UIImage imageWithData:imageData];
                        cell.ivAvatar.image = song.background;
                        cell.ivAvatar.contentMode = UIViewContentModeScaleAspectFit;
                    });
                });
                
            }
        }
        
    }  else {
        cell.hidden = YES;
    }
    
    return cell;
}
- (BOOL) songIsStreamAble: (NSDictionary*) song{
    NSDictionary *source = [song objectForKey:@"source"];
    NSString *link = source[@"128"];
    return link != nil
    && ![link isEqual:[NSNull null]]
    && [link containsString:@"http"];
}

/*---------------------------------------*/
#pragma mark - RIGHT UTILITY BUTTON HANDLERS

- (NSArray *)rightUtilityButtons {
    NSArray *btnArray;
    btnArray = @[[self getButtonWithImage:@"icon-play" ],[self getButtonWithImage:@"icon-top-music-download"]];
    return btnArray;
}
- (UIButton *)getButtonWithImage:(NSString *)imgName  {
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 90, 44)];
    [btn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.4]];
    [btn setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    return btn;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index;
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.currentIndexPath = [_contentTable indexPathForCell:cell];
        _currentIndex = self.currentIndexPath.row;
        [cell hideUtilityButtonsAnimated:YES];
        if (index == 0) {
            [self rightUtiPlay];
        }
        else if (index == 1)
        {
            [self rightUtiDownload];
            
        }
    });
}
- (void) rightUtiPlay {
    UIAppDelegate.soundArray  =  [NSArray arrayWithArray:_listOnlineSongArray];
    HomeViewController *home = [self.tabBarController.viewControllers objectAtIndex:0];
    [self.tabBarController setSelectedViewController:home];
    [home playSongAtIndex:(int) _currentIndex];
    
}
- (void) rightUtiDownload {
    OnlineSong *item = [_listOnlineSongArray objectAtIndex:_currentIndexPath.row];
    if ([Common isCurrentSongDowloaded:item.songId] ) {
        [Common showErrorWithMessage:@"Bài hát này đã được tải về máy"];
        return;
    }
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString: item.link_download ]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        self.loadingSongData = [NSMutableData data];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    else {
        UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"No Network"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [didFailWithErrorMessage show];
    }
}

/*------------------------*/
#pragma mark - Handle downloading interface

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.loadingSongData appendData:data];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    connection = nil;
    self.loadingSongData = nil;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"didFailWithError"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    [didFailWithErrorMessage show];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    OnlineSong *song = [_listOnlineSongArray objectAtIndex: (int)_currentIndex];
    NSString *songStore = [NSString stringWithFormat:@"%@.mp3", song.songId];
    NSString *folderPath = [UIAppDelegate.DIRECTORYPATH stringByAppendingPathComponent:songStore];
    [[NSFileManager defaultManager] createFileAtPath:folderPath
                                            contents:self.loadingSongData
                                          attributes:nil];
    if ([[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        LocalSong *dowload = [[LocalSong alloc] initWithSongId:song.songId title:song.title artist:song.artist duration:song.duration background:song.background lyrics:song.lyrics];
        [UIAppDelegate.localSongArray addObject:dowload];
        [NSKeyedArchiver archiveRootObject:UIAppDelegate.localSongArray toFile:UIAppDelegate.LISTOFFLINEPATH];
    }
}

@end
