
//
//  TrendingViewController.m
//  Music App
//
//  Created by Vuong Hoang Son on 5/15/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "TrendingViewController.h"
#import "TrendingTableViewCell.h"
#import "LocalSong.h"
#import "AppDelegate.h"

@interface TrendingViewController ()
@property(nonatomic,assign) NSInteger currentIndex;
@end

@implementation TrendingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePlayList)
                                                 name:@"IndexChanged"
                                               object:nil];
//    [self updateUIForCurrentSong];
}
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    [self.contentTable reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [UIAppDelegate.localSongArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ([UIAppDelegate.localSongArray count] == 0)?0:1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TrendingTableViewCell *cell = (TrendingTableViewCell *)[self.contentTable dequeueReusableCellWithIdentifier:@"TrendingCell"];
    if (indexPath.row == UIAppDelegate.currentPlayingIndex) {
        cell.lblSerial.hidden = YES;
        cell.imgPlay.hidden = NO;
    } else {
        cell.lblSerial.hidden = NO;
        cell.imgPlay.hidden = YES;
        cell.lblSerial.text = [NSString stringWithFormat:@"%ld",(long)(indexPath.row + 1)];
    }
    if ([[UIAppDelegate.localSongArray objectAtIndex:indexPath.row] isKindOfClass:[LocalSong class]]) {
        LocalSong *song = [UIAppDelegate.localSongArray objectAtIndex:indexPath.row];
        NSLog(@"%@", song.duration);
        cell.lblTime.text = [self formatTime:song.duration];
        cell.lblTrack.text = song.title;
    }

    return cell;
}
- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIAppDelegate.currentPlayingIndex = indexPath.row;
    _currentIndex = indexPath.row;
    UIAppDelegate.soundArray = UIAppDelegate.localSongArray;
    NSDictionary *playingSongAtIndex = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%li", (long)UIAppDelegate.currentPlayingIndex] forKey:@"PlayAtIndex"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MusicPlayingChanged" object:self userInfo:playingSongAtIndex];
    [_contentTable reloadData];
}

- (NSString*) formatTime: (NSString *) duaration {
    int elapsedMinisecond = [duaration intValue];
    NSUInteger h = elapsedMinisecond / 3600;
    NSUInteger m = (elapsedMinisecond / 60) % 60;
    NSUInteger s = elapsedMinisecond % 60;
    if (h > 0){
        return [NSString stringWithFormat:@"%lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
    } else {
        return [NSString stringWithFormat:@"%02lu:%02lu", (unsigned long)m, (unsigned long)s];
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        LocalSong *currentSong = [UIAppDelegate.localSongArray objectAtIndex:indexPath.row];
        NSString *songId  = currentSong.songId;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *folderPath = [UIAppDelegate.DIRECTORYPATH stringByAppendingPathComponent:songId];
        [UIAppDelegate.localSongArray removeObjectAtIndex:indexPath.row];
        UIAppDelegate.soundArray =  UIAppDelegate.localSongArray;
        if ([fileManager fileExistsAtPath:folderPath]) {
              [fileManager removeItemAtPath:folderPath error:nil];
        }
        [self.contentTable reloadData];
        [NSKeyedArchiver archiveRootObject:UIAppDelegate.localSongArray toFile:UIAppDelegate.LISTOFFLINEPATH];

        
    }
}
- (void)updateUIForCurrentSong {
    LocalSong *currentSong = [UIAppDelegate.localSongArray objectAtIndex:_currentIndex];
    [_lblTitleSong setText: currentSong.title];
    [_lblArtist setText:currentSong.artist];
    [_backgroundSong setImage:currentSong.background];
    _backgroundSong.contentMode = UIViewContentModeScaleAspectFit;
}

- (void) updatePlayList {
    [_contentTable reloadData];
    [self updateUIForCurrentSong];
}

@end
