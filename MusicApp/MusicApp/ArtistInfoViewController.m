//
//  ArtistInfoViewController.m
//
//  Created by Vuong Hoang Son on 5/16/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "ArtistInfoViewController.h"



@interface ArtistInfoViewController ()

@end

@implementation ArtistInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self trackList: [NSString stringWithFormat:@"http://api.mp3.zing.vn/api/mobile/artist/getartistinfo?requestdata={\"id\":\"%@\"}", _artistId]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) trackList: (NSString*) url{
    if (![Common checkInternetConnection]) {
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [SCRequest performMethod:SCRequestMethodGET
                  onResource:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
             usingParameters:nil
                 withAccount:[SCSoundCloud account]
      sendingProgressHandler:nil
             responseHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 
                 NSError *jsonError;
                 NSDictionary *responseResult;
                 if (data)  responseResult = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                 else {
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     return;
                     
                 }
                 if ((!jsonError) && responseResult) {
                    // set data return here
                     [self updateArtistInfoFromDict:responseResult];
                 }
                 else {
                     NSLog(@"%@", error.localizedDescription);
                 }
             }];
}
-(void) updateArtistInfoFromDict:(NSDictionary *) dict {
    _artistName.text = dict[@"name"];
    _artistBirthday.text = dict[@"birthday"];
    _artistLocation.text = dict[@"national_name"];
    _artistInfo.text = dict[@"biography"];
    _artistInfo.textColor = [UIColor whiteColor];
    _artistInfo.font = [UIFont systemFontOfSize:13];
    
    NSString *linkAva = [NSString stringWithFormat:@"%@/%@",Z_DOMAIN_IMAGE ,dict[@"avatar"]];
    NSURL *imageURL = [NSURL URLWithString:linkAva];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!imageData)
            {
                _artisAvatar.image = [UIImage imageNamed:@"slide-img-round"];
            }
            else
            {
                _artisAvatar.image = [UIImage imageWithData:imageData];
            }
        });
        CGSize size = [_artisAvatar bounds].size;
        _artisAvatar.layer.cornerRadius = size.width /2;
        _artisAvatar.clipsToBounds = YES;
    });

}

- (IBAction)backToArtistProfileView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}
@end
