

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "UIImageView+AFNetworking.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "LocalSong.h"
#import "Common.h"
#import "OnlineSong.h"

@import MediaPlayer;

@interface HomeViewController ()
@property (nonatomic, assign) BOOL paused;
@property (nonatomic, strong) NSMutableDictionary *nowPlayingInfo;
@property (nonatomic, strong) NSArray *shuffleSongArray;
@property (nonatomic, assign) int totalSong;
@end

@implementation HomeViewController
@synthesize avPlayer;
@synthesize streamingItem, nowPlayingInfo, totalSong;

#pragma mark - ViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMusicPlayingControl:)
                                                 name:@"MusicPlayingChanged"
                                               object:nil];
    [self.playingProgress setThumbImage: [UIImage imageNamed:@"player-scrubber.png"] forState:UIControlStateNormal];
    self.playingProgress.value = 0;
    self.playingProgress.minimumValue = 0.0;
    [self.playingProgress addTarget:self
                             action:@selector(sliderDidEndSliding:)
                   forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchUpOutside |  UIControlEventTouchDragExit | UIControlEventTouchCancel)];
    
    [self.playingProgress addTarget:self action:@selector(sliderWillSliding:) forControlEvents:UIControlEventTouchDragInside];
    
    self.playingProgress.continuous = NO;
    
    self.contentScroll.contentSize = CGSizeMake(784, self.contentScroll.frame.size.height);
    self.contentScroll.contentOffset = CGPointMake(784/2 - self.contentScroll.frame.size.width/2, 0);

    if ((currentIndex < 0) || (currentIndex > appDelegate.soundArray.count-1)) {
        currentIndex = 0;
        UIAppDelegate.currentPlayingIndex = 0;
    }
    nowPlayingInfo = [[NSMutableDictionary alloc] init];
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(checkPlaybackTime:)
                                   userInfo:nil
                                    repeats:YES];
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    
    self.avPlayer = [[AVPlayer alloc]init];
    //    [self.avPlayer addObserver:self forKeyPath:@"rate" options:0 context:nil];
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}
-(void)viewDidAppear:(BOOL)animated {
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

#pragma HANDLE PLAYER
-(void)playSongAtIndex:(int)index{
    currentIndex = index;
    UIAppDelegate.currentPlayingIndex = index;
    [self initializePlayer];
}
-(void)initializePlayer {
    if ([UIAppDelegate.soundArray count] == 0) {
        [Common showWarningWithMessage:@"Không có bài hát nào được chọn"];
        return;
    }
    if ([[UIAppDelegate.soundArray objectAtIndex:currentIndex] isEqual:[NSNull class]]){
        [Common showWarningWithMessage:@"Không thể chơi nhạc"];
        return;
    }
    NSURL *url ;
    if (avPlayer != nil){
        [avPlayer pause];
    }
    if ([[UIAppDelegate.soundArray objectAtIndex:currentIndex] isKindOfClass:[LocalSong class]]) {
        LocalSong *song = [UIAppDelegate.soundArray objectAtIndex:currentIndex];
        
        NSString *folderPath = [UIAppDelegate.DIRECTORYPATH stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3", song.songId]];
        if ([[NSFileManager defaultManager] fileExistsAtPath:folderPath])
        {
            url = [NSURL fileURLWithPath:folderPath];
        }
        UIAppDelegate.currentTitleSong = song.title;
        UIAppDelegate.currentSongArtist = song.artist;
    }
    else if (UIAppDelegate.isUsingZingAPI) {
        OnlineSong *song = [UIAppDelegate.soundArray objectAtIndex:currentIndex];
        url = [NSURL URLWithString: song.link_play];
        NSLog(@"%@", url);
    }
    else
    {
        url = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"%@?client_id=%@", [appDelegate.soundArray objectAtIndex:currentIndex][@"stream_url"], @"05f12042d51e307cc1488b504fdc0646"]];
    }
    NSLog(@" url stream:  %@", url.scheme);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.streamingItem];
    self.avPlayer = [AVPlayer playerWithURL:url];
    [self playAudio];
    avPlayer.volume = volumneSlider.value;
    
    if([url.scheme isEqualToString:@"http"]) {
        [self updateUI];
    } else
    {
        self.currentAsset = [AVURLAsset assetWithURL: url];
        self.streamingItem = [AVPlayerItem playerItemWithAsset:self.currentAsset];
        [self updateUIForLocalSong];
    }
    [self updatePlayingInfoCenter];
    
}
-(void)updateUI {
    OnlineSong *song = [UIAppDelegate.soundArray objectAtIndex: currentIndex];
    lblTrack.text = song.title;
    lblArtist.text = song.artist;
    UIAppDelegate.currentTitleSong = song.title ;
    UIAppDelegate.currentSongArtist = song.artist ;
    if (![song.link_thumbnail containsString:@"http"]) {
        [imgSong setImage:[UIImage imageNamed:@"slide-img-round"]];
        [imgSong1 setImage:[UIImage imageNamed:@"slide-img-round"]];
        [imgSong2 setImage:[UIImage imageNamed:@"slide-img-round"]];
         UIAppDelegate.currentBackgroundSong = [UIImage imageNamed:@"slide-img-round"];
     
    }
    else {
        NSURL *imageURL = [NSURL URLWithString:song.link_thumbnail];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            if (!imageData)
            {
                song.background = [UIImage imageNamed:@"slide-img-round"];
            }
            else
            {
                song.background = [UIImage imageWithData:imageData];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAppDelegate.currentBackgroundSong = song.background;
                [imgSong setImage:song.background];
                [imgSong1 setImage:song.background];
                [imgSong2 setImage:song.background];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"IndexChanged" object:self userInfo:nil];
            });
        });
    }
}
-(void)updateUIForLocalSong {
    dispatch_async(dispatch_get_main_queue(), ^{
        LocalSong *local = [UIAppDelegate.soundArray objectAtIndex:currentIndex];
        [self setTextForLabel:lblArtist name:local.title];
        [self setTextForLabel:lblTrack name: local.artist];
        UIAppDelegate.currentBackgroundSong = local.background;
        [imgSong  setImage: local.background];
        [imgSong1 setImage:local.background];
        [imgSong2 setImage:local.background];
        [self updatePlayingInfoCenter];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"IndexChanged" object:self userInfo:nil];
    });
    
}
-(void)checkPlaybackTime:(NSTimer *)theTimer{
    if (avPlayer != nil && avPlayer.rate == 1.0){
        int currTime = CMTimeGetSeconds(avPlayer.currentItem.currentTime);
        int durationTime = CMTimeGetSeconds(avPlayer.currentItem.duration);
        [self setTextForLabel:lbTotalTime
                         name: [self formatTime:(durationTime * 1000)]];
        [self setTextForLabel:playedTime name: [self formatTime:(currTime * 1000)]];
        self.playingProgress.value = currTime*1.0f/durationTime;
        
    }
}
-(void) updatePlayingInfoCenter {
    
    [nowPlayingInfo setObject: UIAppDelegate.currentSongArtist forKey:MPMediaItemPropertyArtist];
    [nowPlayingInfo setObject:UIAppDelegate.currentTitleSong forKey:MPMediaItemPropertyTitle];
    [nowPlayingInfo setObject:[NSNumber numberWithDouble:avPlayer.rate] forKey:MPNowPlayingInfoPropertyPlaybackRate];
    [nowPlayingInfo setObject:@([self currentPlaybackDuration]) forKey: MPMediaItemPropertyPlaybackDuration];
    [nowPlayingInfo setObject:@([self currentPlaybackTime])forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    if (UIAppDelegate.currentBackgroundSong) {
        MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc] initWithImage: UIAppDelegate.currentBackgroundSong];
        [artwork imageWithSize:CGSizeMake(300, 300)];
        [nowPlayingInfo setValue:artwork forKey:MPMediaItemPropertyArtwork];
    }
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nowPlayingInfo;
}
- (NSTimeInterval)currentPlaybackDuration
{
    return CMTimeGetSeconds([[avPlayer.currentItem asset] duration]);
}

#pragma HANDLE AVPLAYER BUTTON CLICK - PLAY, PAUSE, NEXT..ect
- (void)playAudio {
    [avPlayer play];
    btnPlay.selected = YES;
    _paused = NO;
    [nowPlayingInfo setObject: @([avPlayer rate]) forKey:MPNowPlayingInfoPropertyPlaybackRate];
    [nowPlayingInfo setObject:@([self currentPlaybackTime])forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nowPlayingInfo;
}
- (void)pauseAudio {
    //Pause the audio and set the button to represent the audio is paused
    [avPlayer pause];
    btnPlay.selected = NO;
    _paused = YES;
    [nowPlayingInfo setObject: @([avPlayer rate]) forKey:MPNowPlayingInfoPropertyPlaybackRate];
    [nowPlayingInfo setObject:@([self currentPlaybackTime])forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nowPlayingInfo;
}
- (void)togglePlayPause {
    if (avPlayer.rate > 0 && avPlayer.error == nil) {
        [self pauseAudio];
    }
    else if (avPlayer && _paused ) {
        [self playAudio];
    }
    else {
        [self playSongAtIndex:currentIndex];
    }
}
- (void) nextBtnTapped {
    if ([UIAppDelegate.soundArray count] == 1) {
        [self playSongAtIndex:0];
    }
    else if ([UIAppDelegate.soundArray count] > 1) {
        if (currentIndex < [UIAppDelegate.soundArray count] - 2) {
            [self playSongAtIndex:currentIndex + 1];
            return;
        }
        else [self playSongAtIndex:0];
    }
    return;
}
- (void) previousBtnTapped {
    if ([UIAppDelegate.soundArray count]== 1) {
        [self playSongAtIndex:0];
    }
    else if ([UIAppDelegate.soundArray count] > 1) {
        if (currentIndex > 0) {
            [self playSongAtIndex:currentIndex - 1];
            return;
        }
        [self playSongAtIndex:0];
    }
    return;
}

#pragma HANDLE ACTION BUTTON CLICK
-(IBAction)togglePlayer:(id)sender {
    [self togglePlayPause];
}
- (IBAction)nextBtnTapped:(id)sender {
    [self nextBtnTapped];
}
- (IBAction)previousBtnTapped:(id)sender{
    [self previousBtnTapped];
}
- (IBAction)changeVolume:(id)sender {
    if (avPlayer != nil){
        avPlayer.volume = volumneSlider.value;
    }
}

#pragma mark - Slide timer

- (void)sliderDidEndSliding:(id)sender {
    CMTime newTime = CMTimeMakeWithSeconds(self.playingProgress.value * [self durationInSeconds], 1);
    [avPlayer seekToTime:newTime];
    [self playAudio];
}
- (void)sliderWillSliding:(id)sender {
    //Stop playing
    [avPlayer pause];
    
}
- (Float64)durationInSeconds {
    
    Float64 dur = CMTimeGetSeconds(avPlayer.currentItem.duration);
    return dur;
}

#pragma mark - Update UI Player
- (void)setTextForLabel:(UILabel *)lbl name:(NSString *)name {
    if (![name isKindOfClass:[NSNull class]]){
        [lbl setText:name];
    }
}
- (void)setImageForView:(UIImageView *)img name:(NSString *)name {
    if (name != nil
        && ![name isEqual: [NSNull null]]
        && [name containsString:@"http"]){
        NSLog(@"Image url : %@", [name stringByReplacingOccurrencesOfString:@"-large." withString:@"-t300x300."]);
        NSURL *imageURL = [NSURL URLWithString:[name stringByReplacingOccurrencesOfString:@"-large." withString:@"-t300x300."]];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                img.image = [UIImage imageWithData:imageData];
            });
        });
        
    }
}
- (NSString*) formatTime: (int) elapsedMinisecond{
    elapsedMinisecond = elapsedMinisecond / 1000;
    NSUInteger h = elapsedMinisecond / 3600;
    NSUInteger m = (elapsedMinisecond / 60) % 60;
    NSUInteger s = elapsedMinisecond % 60;
    if (h > 0){
        return [NSString stringWithFormat:@"%lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
    } else {
        return [NSString stringWithFormat:@"%02lu:%02lu", (unsigned long)m, (unsigned long)s];
    }
}

- (NSTimeInterval)currentPlaybackTime
{
    return avPlayer.currentTime.value == 0 ? 0 : avPlayer.currentTime.value / avPlayer.currentTime.timescale;
}

- (BOOL) songIsStreamAble: (NSDictionary*) song{
    if (song == nil || [song isEqual:[NSNull null]]) return NO;
    NSString *link = song[@"stream_url"];
    return link != nil
    && ![link isEqual:[NSNull null]]
    && [link containsString:@"http"];
}
//Make sure we can recieve remote control events
- (BOOL)canBecomeFirstResponder {
    return YES;
}
-(void)handleMusicPlayingControl: (NSNotification *) change {
    NSDictionary *theData = [change userInfo];
    if (theData != nil) {
        NSString *control = [theData objectForKey:@"playingChanged"];
        if (control) {
            if ([control isEqualToString:@"play"]) {
                [self playAudio];
            } else if ([control isEqualToString:@"pause"]) {
                [self pauseAudio];
            } else if ([control isEqualToString:@"toggle"]) {
                [self togglePlayPause];
            } else if ([control isEqualToString:@"next"]) {
                [self nextBtnTapped];
            } else if ([control isEqualToString:@"prev"]) {
                [self previousBtnTapped];
            }
        }
        else {
            NSString *control = [theData objectForKey:@"PlayAtIndex"];
            if (control) {
                int index = [control intValue];
                [self playSongAtIndex:index];
            }
        }
    }
}
-(void)itemDidFinishPlaying:(NSNotification *) notification {
    [self nextBtnTapped:nil];
}

@end
