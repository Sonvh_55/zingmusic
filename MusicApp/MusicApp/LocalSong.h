//
//  LocalSong.h
//  SoundFree
//
//  Created by PC2 on 4/28/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LocalSong : NSObject <NSCoding>
@property(nonatomic, strong) NSString *songId;
@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSString *artist;
@property(nonatomic, strong) NSString *duration;
@property(nonatomic, strong) UIImage *background;
@property(nonatomic, strong) NSString *lyrics;

-(id)initWithSongId: (NSString *)songId title: (NSString *) title artist: (NSString *) artist duration: (NSString *) duration;
-(id)initWithSongId: (NSString *)songId title: (NSString *) title artist: (NSString *) artist duration: (NSString *) duration background: (UIImage *)image lyrics: (NSString *) lyrics;

@end
