//
//  LocalSong.m
//  SoundFree
//
//  Created by PC2 on 4/28/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "LocalSong.h"
#import "AppDelegate.h"

@implementation LocalSong

-(id)initWithSongId: (NSString *)CsongId title: (NSString *) Ctitle artist: (NSString *) Cartist duration:(NSString *)Cduration{
    self = [super init];
    self.songId = CsongId;
    self.title = Ctitle;
    self.artist = Cartist;
    self.duration = Cduration;
    return  self;
}
-(id)initWithSongId: (NSString *)songId title: (NSString *) title artist: (NSString *) artist duration: (NSString *) duration background: (UIImage *)image lyrics: (NSString *) lyrics {
    self = [super init];
    self.songId =  songId;
    self.title = title;
    self.artist = artist;
    self.duration = duration;
    self.background = image;
    self.lyrics = lyrics;
    return  self;

}
- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.songId forKey:@"songId"];
    [encoder encodeObject:self.artist forKey:@"artist"];
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.duration forKey:@"duration"];
    [encoder encodeObject:self.background forKey:@"background"];
    [encoder encodeObject:self.lyrics forKey:@"lyrics"];

}
- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if( self != nil )
    {
        self.songId = [decoder decodeObjectForKey:@"songId"];
        self.artist = [decoder decodeObjectForKey:@"artist"];
        self.title = [decoder decodeObjectForKey:@"title"];
        self.duration = [decoder decodeObjectForKey:@"duration"];
        self.background = [decoder decodeObjectForKey:@"background"];
        self.lyrics = [decoder decodeObjectForKey:@"lyrics"];
        
    }
    return self;
}
@end
