//
//  TrendingViewController.h
//  Music App
//
//  Created by Vuong Hoang Son on 5/15/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface TrendingViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *contentTable;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundSong;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSong;
@property (weak, nonatomic) IBOutlet UILabel *lblArtist;
@property (weak, nonatomic) IBOutlet UILabel *lblAlbum;
@property (weak, nonatomic) IBOutlet UILabel *lblShuffle;
@end
