//
//  TrendingTableViewCell.h
//  MusicApp
//
//  Created by Vuong Hoang Son on 14/05/2015.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "SWTableViewCell.h"

@interface TrendingTableViewCell : SWTableViewCell <SWTableViewCellDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *imgPlay;
@property (nonatomic, weak) IBOutlet UILabel *lblSerial;
@property (nonatomic, weak) IBOutlet UILabel *lblTrack;
@property (nonatomic, weak) IBOutlet UILabel *lblTime;

@end
