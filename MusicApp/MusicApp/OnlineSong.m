//
//  OnlineSong.m
//  SoundFree
//
//  Created by Huy Hoang on 5/8/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "OnlineSong.h"
#import "AppDelegate.h"

@implementation OnlineSong

- (id)initSongWithZingListDictionary:(NSDictionary *)zing {
    self = [super init];
    if(self && [zing isKindOfClass:[NSDictionary class]]) {
        self.title = [[zing objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        if ([[zing objectForKey:@"song_id"] isKindOfClass:[NSString class]]) {
            self.songId = [zing objectForKey:@"song_id"];
        } else self.songId = [[zing objectForKey:@"song_id"] stringValue];
        
        self.artist = [[zing objectForKey:@"artist"] stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        NSNumber *duarationNumber = [zing objectForKey:@"duration"];
        
        self.duration =[duarationNumber stringValue];
        self.linkShare= [zing objectForKey:@"link"];
        self.link_play = [zing objectForKey:@"source"][@"128"];
        self.link_download = [zing objectForKey:@"source"][@"128"];
        self.link_lyrics = [zing objectForKey:@"lyrics_file"];
        self.total_play = [[zing objectForKey:@"total_play"] stringValue];
        self.link_thumbnail = [zing objectForKey:@"thumbnail"];
        
        self.linkShare = [NSString stringWithFormat:@"%@%@", Z_DOMAIN_MP3, self.linkShare];
        self.link_thumbnail = [NSString stringWithFormat:@"%@/%@", Z_DOMAIN_IMAGE, self.link_thumbnail];
        NSURL *linkLyrics = [NSURL URLWithString:self.link_lyrics ];
        self.lyrics = [NSString stringWithContentsOfURL: linkLyrics encoding: NSUTF8StringEncoding error:nil];
    }
    return self;
    
}
- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.songId forKey:@"songId"];
    [encoder encodeObject:self.artist forKey:@"artist"];
    [encoder encodeObject:self.title forKey:@"title"];
    [encoder encodeObject:self.duration forKey:@"duration"];
    [encoder encodeObject:self.background forKey:@"background"];
    [encoder encodeObject:self.linkShare forKey:@"linkShare"];
    [encoder encodeObject:self.link_play forKey:@"link_play"];
    [encoder encodeObject:self.link_download forKey:@"link_download"];
    [encoder encodeObject:self.link_lyrics forKey:@"link_lyrics"];
    [encoder encodeObject:self.total_play forKey:@"total_play"];
    [encoder encodeObject:self.link_thumbnail forKey:@"link_thumbnail"];
    [encoder encodeObject:self.lyrics forKey:@"lyrics"];
    
}
- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if( self != nil )
    {
        self.songId = [decoder decodeObjectForKey:@"songId"];
        self.artist = [decoder decodeObjectForKey:@"artist"];
        self.title =  [decoder decodeObjectForKey:@"title"];
        self.duration = [decoder decodeObjectForKey:@"duration"];
        self.background = [decoder decodeObjectForKey:@"background"];
        self.lyrics = [decoder decodeObjectForKey:@"lyrics"];
        self.linkShare  = [decoder  decodeObjectForKey:@"linkShare"];
        self.link_play = [decoder  decodeObjectForKey:@"link_play"];
        self.link_download = [decoder decodeObjectForKey:@"link_download"];
        self.link_lyrics = [decoder decodeObjectForKey:@"link_lyrics"];
        self.total_play = [decoder decodeObjectForKey:@"total_play"];
        self.link_thumbnail = [decoder decodeObjectForKey:@"link_thumbnail"];
        
    }
    return self;
}

-(NSString *) toString {
    NSLog(@"StartLog");
    NSLog(@"%@", self.title);
    NSLog(@"%@", self.songId);
    NSLog(@"%@", self.artist);
    NSLog(@"%@", self.duration);
    NSLog(@"%@", self.linkShare);
    NSLog(@"%@", self.link_play);
    NSLog(@"%@", self.link_download);
    NSLog(@"%@", self.link_lyrics);
    NSLog(@"%@", self.total_play);
    NSLog(@"%@", self.link_thumbnail);
    NSLog(@"EndLog");
    return @"LOG";
}

@end
