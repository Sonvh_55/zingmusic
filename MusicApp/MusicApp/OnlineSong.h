//
//  OnlineSong.h
//  SoundFree
//
//  Created by Huy Hoang on 5/8/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OnlineSong : NSObject <NSCoding>

@property (nonatomic, strong) NSString *songId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *artist;
@property (nonatomic, strong) NSString *duration;
@property (nonatomic, strong) NSString *linkShare;
@property (nonatomic, strong) NSString *link_play;
@property (nonatomic, strong) NSString *link_download;
@property (nonatomic, strong) NSString *link_lyrics;
@property (nonatomic, strong) NSString *lyrics;
@property (nonatomic, strong) NSString *total_play;
@property (nonatomic, strong) NSString *link_thumbnail;
@property (nonatomic, strong) UIImage  *background;

- (id)initSongWithZingListDictionary:(NSDictionary *)dict;
- (NSString *) toString;
@end
