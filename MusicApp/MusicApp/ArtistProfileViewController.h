//
//  ArtistProfileViewController.h
//
//  Created by Vuong Hoang Son on 5/16/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "SCUI.h"
#import "SCAPI.h"
#import "SWTableViewCell.h"
#import "ArtistInfoViewController.h"

@interface ArtistProfileViewController : UIViewController
@property (strong, nonatomic) NSString *artistId;
@property (strong, nonatomic) NSString *artistAvaLink;
@property (strong, nonatomic) NSString *artistName;
@property (weak, nonatomic) IBOutlet UIImageView *ivAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbArtistName;
@property (nonatomic, weak) IBOutlet UITableView *contentTable;
@property (weak, nonatomic) IBOutlet UIView *networkErrorView;
- (IBAction)backToArtistView:(id)sender;
- (IBAction)reloadNetworkButtonTapped:(id)sender;
@end


