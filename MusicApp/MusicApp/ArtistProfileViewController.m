//
//  ArtistProfileViewController.m
//
//  Created by Vuong Hoang Son on 5/16/15.
//  Copyright (c) 2015 MyAppTemplates. All rights reserved.
//

#import "ArtistProfileViewController.h"
#import "TrendingTableViewCell.h"
#include "HomeViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "Common.h"
#import "LocalSong.h"
#import "OnlineSong.h"

@interface ArtistProfileViewController () <SWTableViewCellDelegate>
@property(strong, nonatomic) NSMutableArray *listSongOfArtistArray;
@property (nonatomic, assign) NSIndexPath *currentIndexPath;
@property (nonatomic, strong) NSMutableData *loadingSongData;
@end

static NSString *artistInfoIndentifier = @"artistInfo";
@implementation ArtistProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _listSongOfArtistArray = [[NSMutableArray alloc] init];
    SWTableViewCell *swTableView = [SWTableViewCell new];
    swTableView.delegate =  self;
    [self updateCoverPhoto];
    [self trackList: [NSString stringWithFormat:@"http://api.mp3.zing.vn/api/mobile/artist/getsongofartist?requestdata={\"length\":20,\"id\":\"%@\",\"start\":0}", _artistId]];
    [_lbArtistName setText:_artistName];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) trackList: (NSString*) url{
    if (![Common isHasInternetConnection]) {
        _networkErrorView.hidden = NO;
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [SCRequest performMethod:SCRequestMethodGET
                  onResource:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
             usingParameters:nil
                 withAccount:[SCSoundCloud account]
      sendingProgressHandler:nil
             responseHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 
                 NSError *jsonError;
                 NSDictionary *responseResult;
                 if (data)  responseResult = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                 else {
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     return;
                     
                 }
                 if ((!jsonError) && responseResult) {
                     [_listSongOfArtistArray removeAllObjects];
                     _listSongOfArtistArray = [NSMutableArray array];
                     NSArray *list;
                     if (UIAppDelegate.isUsingZingAPI) {
                         list = (NSArray *) [responseResult objectForKey:@"docs"];
                         for (NSDictionary *zingItem in list ){
                                 OnlineSong *song =  [[OnlineSong alloc] initSongWithZingListDictionary:zingItem];
                                 [_listSongOfArtistArray addObject:song];
                         }
                         [self.contentTable reloadData];
                     }
                    list = nil;
                     
                 }
                 else {
                     NSLog(@"%@", error.localizedDescription);
                 }
             }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listSongOfArtistArray count];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ([_listSongOfArtistArray count] == 0)?0:1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self playSongAtIndex:indexPath.row];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TrendingTableViewCell *cell = (TrendingTableViewCell *)[self.contentTable dequeueReusableCellWithIdentifier:@"TrendingCell"];
    cell.delegate = self;
    OnlineSong *item = [_listSongOfArtistArray objectAtIndex:indexPath.row];
    cell.rightUtilityButtons = [self rightUtilityButtons];
    cell.lblTrack.text = item.title;
    return cell;
    
}


/*---------------------------------------*/
#pragma mark - RIGHT UTILITY BUTTON HANDLERS

- (NSArray *)rightUtilityButtons {
    NSArray *btnArray;
    btnArray = @[[self getButtonWithImage:@"icon-play" ],[self getButtonWithImage:@"icon-top-music-download"]];
    return btnArray;
}
- (UIButton *)getButtonWithImage:(NSString *)imgName  {
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 90, 44)];
    [btn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.4]];
    [btn setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    return btn;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index;
{
    self.currentIndexPath = [_contentTable indexPathForCell:cell];
    [cell hideUtilityButtonsAnimated:YES];
    if (index == 0) {
        [self rightUtiPlay];
    }
    else if (index == 1)
    {
//        [self rightUtiLike];
        [self rightUtiDownload];
    }
    else if (index == 2)
    {
        [self rightUtiDownload];
    }
}
- (void) rightUtiPlay {
    [self playSongAtIndex:_currentIndexPath.row];
}
- (void) rightUtiLike {
    NSLog(@"liked");
}
- (void) rightUtiDownload {
    OnlineSong *item = [_listSongOfArtistArray objectAtIndex:_currentIndexPath.row];
    NSURLRequest *theRequest=[NSURLRequest requestWithURL:[NSURL URLWithString: item.link_download]
                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval:60.0];
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    if (theConnection) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    else {
        UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"No Network"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        [didFailWithErrorMessage show];
    }
}
- (void)playSongAtIndex:(NSInteger) currentIndex {
    
    UIAppDelegate.soundArray  = [NSArray arrayWithArray:_listSongOfArtistArray];
    UIAppDelegate.currentPlayingIndex = currentIndex;
    NSDictionary *playingSongAtIndex = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%li", (long)UIAppDelegate.currentPlayingIndex] forKey:@"PlayAtIndex"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MusicPlayingChanged" object:self userInfo:playingSongAtIndex];
}


-(void)updateCoverPhoto {
    if (_artistAvaLink != nil
        && ![_artistAvaLink isEqual: [NSNull null]]
        ){
        NSURL *imageURL = [NSURL URLWithString:_artistAvaLink];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update the UI
                UIImage *background = [UIImage imageWithData:imageData];
                _ivAvatar.image = background;
                // Set image over layer
                CAGradientLayer *gradient = [CAGradientLayer layer];
                gradient.frame = _ivAvatar.frame;
                
                // Add colors to layer
                UIColor *start = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
                UIColor *centerColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
                UIColor *endColor = [UIColor grayColor];
                gradient.colors = [NSArray arrayWithObjects:
                                   (id)start,
                                   (id)[centerColor CGColor],
                                   (id)[endColor CGColor],
                                   nil];
                
                [_ivAvatar.layer insertSublayer:gradient atIndex:0];
            });
        });
    }

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backToArtistView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)reloadNetworkButtonTapped:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if ([Common isHasInternetConnection]) {
        _networkErrorView.hidden = YES;
        [self trackList: [NSString stringWithFormat:@"http://api.mp3.zing.vn/api/mobile/artist/getsongofartist?requestdata={\"length\":20,\"id\":\"%@\",\"start\":0}", _artistId]];
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.loadingSongData appendData:data];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    // release the connection, and the data object
    // receivedData is declared as a method instance elsewhere
    connection = nil;
    self.loadingSongData = nil;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    UIAlertView *didFailWithErrorMessage = [[UIAlertView alloc] initWithTitle: @"NSURLConnection " message: @"didFailWithError"  delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
    [didFailWithErrorMessage show];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    OnlineSong *song = [_listSongOfArtistArray objectAtIndex:_currentIndexPath.row];
    NSString *songStore = [NSString stringWithFormat:@"%@.mp3", song.songId];
    NSString *folderPath = [UIAppDelegate.DIRECTORYPATH stringByAppendingPathComponent:songStore];
    [[NSFileManager defaultManager] createFileAtPath:folderPath
                                            contents:self.loadingSongData
                                          attributes:nil];
    if ([[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        LocalSong *dowload = [[LocalSong alloc] initWithSongId:song.songId title:song.title artist:song.artist duration:song.duration background:song.background lyrics:song.lyrics];
        [UIAppDelegate.localSongArray addObject:dowload];
        [NSKeyedArchiver archiveRootObject:UIAppDelegate.localSongArray toFile:UIAppDelegate.LISTOFFLINEPATH];
    }
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:artistInfoIndentifier])
    {
        ArtistInfoViewController *vc = [segue destinationViewController];
        // Pass any objects to the view controller here, like...
        vc.artistId = _artistId;
        
    }
    
}
@end
